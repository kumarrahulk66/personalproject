from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Registration





@api_view(['POST'])
def add_data(request):
    reg=Registration()
    reg.name=request.data['name']
    reg.msg=request.data['msg']
    reg.save()

    # I am writing one different way to do this below in comment
    # newdata = request.data
    # reg = Registration(**newdata)
    # reg.save()

    return Response("success", status=status.HTTP_200_OK)


@api_view(['GET'])
def get_data(request):
    data_list = Registration.objects.all()
    data = {"getdetails": data_list.values()}
    return Response(data, status=status.HTTP_200_OK)


