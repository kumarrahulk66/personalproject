# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-06-22 10:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Sample',
            new_name='Registration',
        ),
    ]
