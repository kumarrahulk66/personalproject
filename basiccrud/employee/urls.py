from django.conf.urls import url


from .views import *

urlpatterns = [
    url(r'^storedata', add_data),
    url(r'^getdata',get_data),

]
