angular.module('registration').service('registration', function($http) {

    this.savedetail = function(data) {

        return $http({
            'method': 'POST',
            'url': "/storedata/",
            'data': data,
        })
        // .then(function(data) {
            //     console.log("data in service", data);
            //     return data;
            // })

    }

    this.getdetail = function(data) {
        console.log("getdata in service");
        return $http({
            'method': 'GET',
            'url': "/getdata/",
        }).then(function(data) {
            console.log("emplist in service", data);
            return data;
        });
    }


});
