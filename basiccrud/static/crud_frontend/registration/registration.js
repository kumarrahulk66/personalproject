angular.module('registration', ['ui.bootstrap', 'ui.router', 'ngAnimate']);

angular.module('registration').config(function($stateProvider) {

    /* Add New States Above */
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'registration/partial/registration/registration.html',
        controller: 'RegistrationCtrl'
    });
});
